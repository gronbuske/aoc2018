#!/bin/bash

input=$(cat input1)\n
output=0
for row in $input; do
	operator=${row:0:1}
	number=${row:1:-1}
	if [ $operator == '+' ]
	then
		output=$(($output+$number))
	else
		output=$(($output-$number))
	fi
done
echo "First output: $output"

notDone=true
output=0
duplicates=()
while [ $notDone = true ]; do
	for row in $input; do
		operator=${row:0:1}
		number=${row:1:-1}
		if [ $operator == '+' ]
		then
			output=$(($output+$number))
		else
			output=$(($output-$number))
		fi
		if [[ " ${duplicates[*]} " =~ " $output " ]]
		then
			notDone=false
			break
		elif (($output > 0)) && (($output < 1000)); then
			duplicates+=($output)
		fi
	done
done
echo "Second output: $output"
