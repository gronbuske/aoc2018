import re

pixels = []
highestY = 0
lowestY = 0
input = open('input10', 'r')
for line in input:
    m = re.search('position=<(.*), (.*)> velocity=<(.*), (.*)>', line)
    x = int(m.group(1))
    y = int(m.group(2))
    vx = int(m.group(3))
    vy = int(m.group(4))
    pixels.append([x, y, vx, vy])
    highestY = max(highestY, y)
    lowestY = min(lowestY, y)
input.close()

highestX = 0
lowestX = 0
nextHighestY = lowestY
nextLowestY = highestY
counter = 0
while highestY > nextHighestY and lowestY < nextLowestY:
    counter += 1
    nextHighestY = lowestY
    nextLowestY = highestY
    highestY = nextHighestY
    lowestY = nextLowestY
    highestX = -100000
    lowestX = 100000
    for pixel in pixels:
        pixel[0] += pixel[2]
        pixel[1] += pixel[3]
        highestX = max(highestX, pixel[0])
        lowestX = min(lowestX, pixel[0])
        highestY = max(highestY, pixel[1])
        lowestY = min(lowestY, pixel[1])
        nextHighestY = max(nextHighestY, pixel[1] + pixel[3])
        nextLowestY = min(nextLowestY, pixel[1] + pixel[3])

file = open("output", 'w')
for y in range(lowestY - 1, highestY + 1):
    line = ""
    for x in range(lowestX, highestX):
        character = '-'
        for pixel in pixels:
            if pixel[0] == x and pixel[1] == y:
                character = "X"
        line += character
    line += '\n'
    file.write(line)
file.close()

# output1 = 0
# print("First output:", output1)
# output2 = 0
print("Second output:", counter)