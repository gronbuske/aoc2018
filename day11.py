def calcBestSquare(maxSize):
    input = 9798
    bestPowerLevel = 0
    grid = [0] * 300
    for y in range(300):
        grid[y] = [0] * 300
        for x in range(300):
            grid[y][x] = int(((x + 11) * (y + 1) + input) * (x + 11) / 100) % 10 - 5
            for size in range(min(maxSize, min(x, y))):
                tempPowerLevel = 0
                for i in range(y - size, y + 1):
                    tempPowerLevel += sum(grid[i][x - size:x + 1])
                if tempPowerLevel > bestPowerLevel:
                    bestPowerLevel = tempPowerLevel
                    bestSquare = (x - size + 1, y - size + 1, size + 1)
    return bestSquare

print("First output:", calcBestSquare(3))
print("Second output:", calcBestSquare(30))