import re

input = open('input12', 'r')
m = re.search("initial state: (.*)", input.readline())
state = m.group(1)
input.readline()
regexes = []
for line in input:
    m = re.search("(.*) => [#]", line)
    if m != None:
        regexes.append(re.compile("(?=" + re.escape(m.group(1)) + ")"))
input.close()

zero = 0
lastscore = 0
lastdiff = 0
for i in range(10000):
    score = 0
    firstflower = state.find("#")
    if firstflower < 4:
        state = "...." + state
        zero += 4
    if "#" in state[-4:-1]:
        state += "...."
    stringArray = ["."] * len(state)
    for regex in regexes:
        for m in regex.finditer(state):
            stringArray[m.start() + 2] = "#"
            score += m.start() + 2 - zero
    state = "".join(stringArray)
    diff = score - lastscore
    if i == 19:
        print("First output:", score)
    if lastdiff == diff:
        print("Second output:", score + diff * (50000000000 - i - 1))
        break
    lastdiff = diff
    lastscore = score