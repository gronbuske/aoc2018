input = open('input13', 'r')
grid = []
carts = {}
directions = ['<','>','^','v']
currentletter = 'a'
y = 0
for line in input:
    row = list(line)
    for x in range(len(row)):
        if row[x] in directions:
            if x != 0 and row[x - 1] in ["-", "+", "/", "\\"]:
                belowChar = "-"
            else:
                belowChar = "|"
            carts[currentletter] = [row[x], 0, belowChar, "l", x, y]
            row[x] = currentletter
            currentletter = chr(ord(currentletter) + 1) 
    grid.append(row)
    y += 1
input.close()

def GetDirection(x, y):
    nextX = x
    nextY = y
    if direction == directions[0]:
        nextX = x - 1
    elif direction == directions[1]:
        nextX = x + 1
    elif direction == directions[2]:
        nextY = y - 1
    else:
        nextY = y + 1
    return (nextX, nextY)

firstCrash = True
turn = 1
while len(carts) > 1:
    deadCarts = []
    for ch, cart in carts.items():
        x = cart[4]
        y = cart[5]
        if not ch.isalpha() or cart[1] >= turn:
            continue
        cart[1] = turn
        grid[y][x] = cart[2]
        direction = cart[0]
        nextPos = GetDirection(x, y)
        nextCh = grid[nextPos[1]][nextPos[0]]
        cart[4] = nextPos[0]
        cart[5] = nextPos[1]
        if nextCh.isalpha():
            if firstCrash:
                print("First output:", nextPos[0], nextPos[1])
                firstCrash = False
            grid[nextPos[1]][nextPos[0]] = carts[nextCh][2]
            deadCarts.append(ch)
            deadCarts.append(nextCh)
            carts[nextCh][1] = turn
            continue
        cart[2] = nextCh
        grid[nextPos[1]][nextPos[0]] = ch
        # TODO: Fixa till med Karnaugh
        if nextCh == "+":
            if cart[3] == "l":
                cart[3] = "u"
                if direction == "<":
                    cart[0] = "v"
                elif direction == ">":
                    cart[0] = "^"
                elif direction == "v":
                    cart[0] = ">"
                else:
                    cart[0] = "<"
            elif cart[3] == "u":
                cart[3] = "r"
            else:
                cart[3] = "l"
                if direction == "<":
                    cart[0] = "^"
                elif direction == ">":
                    cart[0] = "v"
                elif direction == "v":
                    cart[0] = "<"
                else:
                    cart[0] = ">"
        elif nextCh == "/":
            if direction == "<":
                cart[0] = "v"
            elif direction == ">":
                cart[0] = "^"
            elif direction == "v":
                cart[0] = "<"
            else:
                cart[0] = ">"
        elif nextCh == "\\":
            if direction == "<":
                cart[0] = "^"
            elif direction == ">":
                cart[0] = "v"
            elif direction == "v":
                cart[0] = ">"
            else:
                cart[0] = "<"
    for cart in deadCarts:
        carts.pop(cart)
    turn += 1

for y in range(len(grid)):
    for x in range(len(grid[y])):
        if grid[y][x].isalpha():
            print("Second output:", x, y)
