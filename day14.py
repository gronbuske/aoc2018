class Elf:
    def __init__(self, index, recipescore):
        self.index = index
        self.score = recipescore

input = 540561
inputString = str(input)
recipes = list("37")

elf1 = Elf(0, int(recipes[0]))
elf2 = Elf(1, int(recipes[1]))
counter = 0
while True:
    newRecipe = elf1.score + elf2.score
    for nr in list(str(newRecipe)):
        recipes.append(nr)
    if counter == input:
        output1 = "".join(recipes[input:input + 10])
    counter += 1
    print("".join(recipes[-len(inputString) - 1:]))
    if inputString in "".join(recipes[-len(inputString) - 1:]):
        print("".join(recipes[-len(inputString) - 1:]))
        output2 = len(recipes) - len(inputString)
        break
    elf1.index = (elf1.index + elf1.score + 1) % len(recipes)
    elf1.score = int(recipes[elf1.index])
    elf2.index = (elf2.index + elf2.score + 1) % len(recipes)
    elf2.score = int(recipes[elf2.index])

print("First output:", output1)
print("Second output:", output2)