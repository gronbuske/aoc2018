#!/bin/bash

output=0

threes=$(cat input2 | grep -E -c '(.)(.*\1){2}')
twos=0
for row in $(cat input2); do
	sortedRow=$(echo $row | grep -o . | sort | tr -d "\n")
	if [ ! -z $(echo $sortedRow | grep -P '(?:^|(.)(?!\1))(.)\2(?!\2)') ] ; then
		twos=$((twos+1))
	fi
done

echo $threes
echo $twos

output=$((threes * twos))

echo "First output: $output"

input=$(cat input2 | sed 's/\n/ /g')
result=$(echo $input | grep -E -o '[ ]([a-z]*)[a-z]([a-z]*)[ ][a-z ]*\1.\2')
result=( $result )

echo ${result[0]}
echo ${result[-1]}