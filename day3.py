import re

xmax = 0
ymax = 0
claims = []
input = open("input3", 'r')
for row in input:
    match = re.compile("(.*) @ (.*),(.*): (.*)x(.*)").match(row)
    newClaim = match.group(1)
    xstart = int(match.group(2))
    ystart = int(match.group(3))
    width = int(match.group(4))
    height = int(match.group(5))
    xmax = max(xmax, xstart + width)
    ymax = max(ymax, ystart + height)
    claims.append([newClaim, xstart, ystart, width, height])

output = 0
grid = [False]*xmax
for i in range(xmax):
    grid[i] = [False] * ymax
for i in range(len(claims) - 1):
    for j in range(i + 1, len(claims)):
        for x in range(max(claims[i][1], claims[j][1]), min(claims[i][1] + claims[i][3], claims[j][1] + claims[j][3])):
            for y in range(max(claims[i][2], claims[j][2]), min(claims[i][2] + claims[i][4], claims[j][2] + claims[j][4])):
                output = output if grid[x][y] else output + 1
                grid[x][y] = True
                claims[i][0] = False
                claims[j][0] = False

print("First output: ", output)

for claim in claims:
    if claim[0]: print("Second output: ", claim)