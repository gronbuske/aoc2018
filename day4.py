import re, datetime, operator

input = open("input4", 'r')
array = []
for row in input:
    match = re.compile("\[(.*)-(.*)-(.*) (.*):(.*)\](.*)").match(row)
    year = int(match.group(1))
    month = int(match.group(2))
    day = int(match.group(3))
    hour = int(match.group(4))
    minute = int(match.group(5))
    message = match.group(6)
    newShift = re.compile(".*#(.*) begins").match(message)
    if newShift:
        value = int(newShift.group(1))
    else:
        fallAsleep = re.compile(".*falls asleep.*").match(message)
        if fallAsleep:
            value = True
        else:
            value = False
    array.append([datetime.datetime(year, month, day, hour, minute), value])

array.sort()

guards = {}
guard = 0
for event in array:
    if event[1] == False:
        if guard not in guards:
            guards[guard] = [0] * 60
        for i in range(fellAsleep.minute, event[0].minute):
            guards[guard][i] += 1
    elif event[1] == True:
        fellAsleep = event[0]
    else:
        guard = event[1]

sleepiestGuard = 0
recordSleep = 0
mostConsistentlySleepyGuard = 0
sleepiestOverallMinute = 0
mostTimesSleptInAMinute = 0
for guard, values in guards.items():
    if max(values) > mostTimesSleptInAMinute:
        mostTimesSleptInAMinute = max(values)
        mostConsistentlySleepyGuard = guard
        for i in range(60): 
            if guards[guard][i] == mostTimesSleptInAMinute: 
                sleepiestOverallMinute = i
    if sum(values) > recordSleep:
        sleepiestGuard = guard
        recordSleep = sum(values)

sleepiestMinute = 0
mostSlept = 0
for i in range(60):
    if guards[sleepiestGuard][i] > mostSlept:
        mostSlept = guards[sleepiestGuard][i]
        sleepiestMinute = i

output = sleepiestGuard * sleepiestMinute

print("First output: ", output)

output = mostConsistentlySleepyGuard * sleepiestOverallMinute

print("Second output: ", output)

# [1, 2, 2, 2, 2, 2, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 5, 6, 5, 5, 5, 6, 6, 6, 8, 8, 8, 9, 9, 9, 10, 11, 11, 11, 10, 9, 10, 10, 9, 10, 10, 10, 11, 11, 11, 11, 12, 14, 15, 17, 19, 18, 15, 13, 9, 6, 6, 7, 2, 0]