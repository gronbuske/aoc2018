#!/bin/bash

input=$(cat input5)
length=0
while (( $length != ${#input} )); do
    length=${#input}
    for i in $(seq 65 90); do 
        UPPER=$(printf "%x\n" $i)
        LOWER=$(printf "%x\n" $(($i + 32)))
        input=$(echo $input | sed "s/[\x$LOWER][\x$UPPER]//g" | sed "s/[\x$UPPER][\x$LOWER]//g")
    done
    echo $input > output
done
echo "First output: $length"

SMALLEST=$length
for i in $(seq 65 90); do 
    REMOVEDUPPER=$(printf "%x\n" $i)
    REMOVEDLOWER=$(printf "%x\n" $(($i + 32)))
    length=0
    tempinput=$input
    tempinput=$(echo $tempinput | sed "s/[\x$REMOVEDLOWER]//g" | sed "s/[\x$REMOVEDUPPER]//g")
    while (( $length != ${#tempinput} )); do
        length=${#tempinput}
        for j in $(seq 65 90); do 
            UPPER=$(printf "%x\n" $j)
            LOWER=$(printf "%x\n" $(($j + 32)))
            tempinput=$(echo $tempinput | sed "s/[\x$LOWER][\x$UPPER]//g" | sed "s/[\x$UPPER][\x$LOWER]//g")
        done
    done
    if (( $length < $SMALLEST )); then
        SMALLEST=$length
        BESTCHAR=$(echo $REMOVEDUPPER | xxd -r -p)
    fi
    echo $(echo $REMOVEDUPPER | xxd -r -p) $length
done
echo Second output: $BESTCHAR: $SMALLEST