input = open('input', 'r')

safeLocations = []
for line in input:
    x = int(line.strip().split(',')[0])
    y = int(line.strip().split(',')[1])
    safeLocations.append([0, x, y])

sizex = 0
sizey = 0
for safeLocation in safeLocations:
    sizex = max(safeLocation[1], sizex)
    sizey = max(safeLocation[1], sizey)

partTwoArea = 0
for x in range(sizex):
    for y in range(sizey):
        totalDistance = 0
        closestSafeLocation = -1
        closestDistance = sizex + sizey
        for i in range(len(safeLocations)):
            distance = abs(safeLocations[i][1] - x) + abs(safeLocations[i][2] - y)
            totalDistance += distance
            if closestDistance > distance:
                closestDistance = distance
                closestSafeLocation = i
            elif closestDistance == distance:
                closestSafeLocation = -1
        if closestSafeLocation >= 0 and (x <= 0 or x >= sizex - 1 or y <= 0 or y >= sizey - 1):
            safeLocations[closestSafeLocation][0] = -1
        elif closestSafeLocation != -1 and safeLocations[closestSafeLocation][0] != -1:
            safeLocations[closestSafeLocation][0] += 1
        if totalDistance < 10000:
            partTwoArea += 1

largestArea = 0
for safeLocation in safeLocations:
    largestArea = max(largestArea, safeLocation[0])

print("First output:", largestArea)

print("Second output:", partTwoArea)
