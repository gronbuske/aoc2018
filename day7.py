import re, string

input = open('input', 'r')

prerequisites={}
prerequisites2={}
timeRequired={}
time=60
for letter in string.ascii_uppercase:
    time += 1
    prerequisites[letter] = []
    prerequisites2[letter] = []
    timeRequired[letter] = time
for line in input:
    m = re.search('Step (.) must be finished before step (.) can begin.', line)
    a = m.group(2)
    b = m.group(1)
    prerequisites[a].append(b)
    prerequisites2[a].append(b)

output = ""
while len(prerequisites) > 0:
    readySteps=[]
    for a in prerequisites:
        if len(prerequisites[a]) == 0:
            readySteps.append(a)
    readySteps.sort()
    doneStep = readySteps[0]
    output += doneStep
    for b in prerequisites:
        if doneStep in prerequisites[b]:
            prerequisites[b].remove(doneStep)
    prerequisites.pop(doneStep, None)

print("First output:", output)

output = ""
totaltime = 0
workers = []
for i in range(5):
    workers.append(['', 0])
while len(prerequisites2) > 0:
    readySteps=[]
    for a in prerequisites2:
        if len(prerequisites2[a]) == 0:
            readySteps.append(a)
    readySteps.sort()
    for readyStep in readySteps:
        for worker in workers:
            if worker[1] == 0:
                worker[0] = readyStep
                worker[1] = timeRequired[readyStep]
                prerequisites2.pop(readyStep, None)
                print("Start", worker)
                break
    
    timeRequiredForNext = 100
    for worker in workers:
        if worker[1] > 0 and timeRequiredForNext > worker[1]:
            print(timeRequiredForNext, '>', worker[1])
            timeRequiredForNext = worker[1]
    doneSteps = []
    totaltime += timeRequiredForNext
    print(timeRequiredForNext)
    for worker in workers:
        if worker[1] > 0:
            worker[1] -= timeRequiredForNext
        if worker[1] <= 0:
            doneSteps.append(worker[0])
            worker[0] = ''
            worker[1] = 0
        print(worker)
    doneSteps.sort()
    print("Finished", doneSteps)
    for doneStep in doneSteps:
        output += doneStep
        for b in prerequisites2:
            if doneStep in prerequisites2[b]:
                prerequisites2[b].remove(doneStep)

print("Second output:", output, totaltime)