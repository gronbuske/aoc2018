def fillNode(arrayIndex):
    global output1
    children = int(array[arrayIndex])
    metadata = int(array[arrayIndex + 1])
    arrayIndex += 2
    childValues = []
    for i in range(children):
        result = fillNode(arrayIndex)
        arrayIndex = result[0]
        childValues.append(result[1])
    value = 0
    for i in range(metadata):
        data = int(array[arrayIndex])
        if children == 0:
            value += data
        elif data > 0 and data <= len(childValues):
            value += childValues[data - 1]
        output1 += data
        arrayIndex += 1
    return (arrayIndex, value)

input = open('input8', 'r')
for line in input:
    array = line.split(' ')
input.close()

output1 = 0
output2 = fillNode(0)[1]
print("First output:", output1)
print("Second output:", output2)