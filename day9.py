import re
class node:
    value = 0
    next = 0
    prev = 0
    def __init__(self, value, next, prev):
        self.value = value
        self.next = next
        self.prev = prev

def play(highestMarble, players):
    currNode = node(0, 0, 0)
    currNode.next = currNode
    currNode.prev = currNode
    marble = 0
    scores = [0] * players
    while marble <= highestMarble:
        for i in range(players):
            marble += 1
            if marble % 23 == 0:
                scores[i] += marble
                removeNode = currNode
                for j in range(7):
                    removeNode = removeNode.prev
                removeNode.prev.next = removeNode.next
                removeNode.next.prev = removeNode.prev
                scores[i] += removeNode.value
                currNode = removeNode.next
            else:
                newNode = node(marble, currNode.next.next, currNode.next)
                newNode.next.prev = newNode
                newNode.prev.next = newNode
                currNode = newNode
            if marble > highestMarble:
                break
    return max(scores)

input = open('input9', 'r')
for line in input:
    m = re.search('(.*) players; last marble is worth (.*) points', line)
    players = int(m.group(1))
    highestMarble = int(m.group(2))
input.close()

output1 = play(highestMarble, players)
print("First output:", output1)
output2 = play(highestMarble * 100, players)
print("Second output:", output2)