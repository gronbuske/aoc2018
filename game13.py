import pygame

TILE_SIZE = 32
TILES_IN_X = 29
TILES_IN_Y = 19
SCREEN_WIDTH = TILES_IN_X * TILE_SIZE
SCREEN_HEIGHT = TILES_IN_Y * TILE_SIZE
size = SCREEN_WIDTH, SCREEN_HEIGHT
screen = pygame.display.set_mode(size)

camera = pygame.rect.Rect(0, 0, TILES_IN_X, TILES_IN_Y)

class Textures:
    def __init__(self):
        self.Ucart = pygame.image.load("textures/Ucart.png")
        self.Dcart = pygame.image.load("textures/Dcart.png")
        self.Lcart = pygame.image.load("textures/Lcart.png")
        self.Rcart = pygame.image.load("textures/Rcart.png")
        self.ground = pygame.image.load("textures/ground.bmp")
        self.Xrail = pygame.image.load("textures/Xrail.bmp")
        self.LRrail = pygame.image.load("textures/LRrail.bmp")
        self.UDrail = pygame.image.load("textures/UDrail.bmp")
        self.LUrail = pygame.image.load("textures/LUrail.bmp")
        self.URrail = pygame.image.load("textures/URrail.bmp")
        self.RDrail = pygame.image.load("textures/RDrail.bmp")
        self.DLrail = pygame.image.load("textures/DLrail.bmp")

    def GetTexture(self, char, railToLeft):
        if char == ' ': return self.ground
        if char == '+': return self.Xrail
        if char == '-': return self.LRrail
        if char == '|': return self.UDrail
        if char == '^': return self.Ucart
        if char == 'v': return self.Dcart
        if char == '<': return self.Lcart
        if char == '>': return self.Rcart
        if char == '/':
            if railToLeft: return self.LUrail
            return self.RDrail
        if char == '\\':
            if railToLeft: return self.DLrail
            return self.URrail
        return self.ground

class Cart:
    def __init__(self, direction, belowChar, x, y):
        self.direction = direction
        self. belowChar = belowChar
        self.nextTurn = "l"
        self.x = x
        self.y = y
        self.dead = False
        self.manual = False

class Game:
    def __init__(self):
        self.tickRate = 100
        self.target = 'a'
        self.grid = []
        self.carts = {}
        self.crashes = []
        self.directions = ['<','>','^','v']
        self.textures = Textures()

        currentletter = 'a'
        inputfile = open('input13', 'r')
        y = 0
        for line in inputfile:
            line = line.strip('\n')
            row = list(line)
            for x in range(len(row)):
                if row[x] in self.directions:
                    if x != 0 and row[x - 1] in ["-", "+", "/", "\\"]:
                        belowChar = "-"
                    else:
                        belowChar = "|"
                    self.carts[currentletter] = Cart(row[x], belowChar, x, y)
                    row[x] = currentletter
                    currentletter = chr(ord(currentletter) + 1) 
            self.grid.append(row)
            y += 1
        inputfile.close()
        self.carts[self.target].manual = True

    def GetDirection(self, x, y, direction):
        nextX = x
        nextY = y
        if direction == self.directions[0]:
            nextX = x - 1
        elif direction == self.directions[1]:
            nextX = x + 1
        elif direction == self.directions[2]:
            nextY = y - 1
        else:
            nextY = y + 1
        return (nextX, nextY)

    def Draw(self):
        screen.fill((0, 0, 0))
        if self.target in self.carts: 
            camera.x = self.carts[self.target].x - camera.width / 2 + 1
            camera.y = self.carts[self.target].y - camera.height / 2 + 1
        if camera.x < 0: camera.x = 0
        if camera.y < 0: camera.y = 0
        maxX = len(self.grid[0]) - camera.width
        maxY = len(self.grid) - camera.height 
        if camera.x >= maxX: camera.x = maxX
        if camera.y >= maxY: camera.y = maxY
        for y in range(camera.height):
            for x in range(camera.width):
                if x + camera.x <= 0: railToLeft = False
                elif self.grid[y + camera.y][x + camera.x  - 1] not in [' ', '|']: railToLeft = True
                else: railToLeft = False
                char = self.grid[y + camera.y][x + camera.x]
                if char.isalpha(): 
                    texture = self.textures.GetTexture(self.carts[char].belowChar, railToLeft)
                    char = self.carts[char].direction
                    screen.blit(texture, pygame.rect.Rect(x * TILE_SIZE, y * TILE_SIZE, TILE_SIZE, TILE_SIZE))
                texture = self.textures.GetTexture(char, railToLeft)
                screen.blit(texture, pygame.rect.Rect(x * TILE_SIZE, y * TILE_SIZE, TILE_SIZE, TILE_SIZE))
        screen.blit(texture, pygame.rect.Rect(-10, -10, TILE_SIZE, TILE_SIZE))
        pygame.display.flip()

    def GameLoop(self):
        deadCarts = []
        for ch, cart in self.carts.items():
            x = cart.x
            y = cart.y
            if not ch.isalpha() or cart.dead: continue
            self.grid[y][x] = cart.belowChar
            direction = cart.direction
            nextPos = self.GetDirection(x, y, direction)
            nextCh = self.grid[nextPos[1]][nextPos[0]]
            cart.x = nextPos[0]
            cart.y = nextPos[1]
            if nextCh.isalpha():
                self.grid[nextPos[1]][nextPos[0]] = self.carts[nextCh].belowChar
                deadCarts.append(ch)
                deadCarts.append(nextCh)
                cart.dead = True
                self.carts[nextCh].dead = True
                continue
            cart.belowChar = nextCh
            self.grid[nextPos[1]][nextPos[0]] = ch
            # TODO: Fixa till med Karnaugh
            if nextCh == "+":
                if cart.nextTurn == "l":
                    cart.nextTurn = "u"
                    if direction == "<": cart.direction = "v"
                    elif direction == ">": cart.direction = "^"
                    elif direction == "v": cart.direction = ">"
                    else: cart.direction = "<"
                elif cart.nextTurn == "u": 
                    if not cart.manual: cart.nextTurn = "r"
                else:
                    if cart.manual: 
                        cart.nextTurn = "u"
                    else:
                        cart.nextTurn = "l"
                    if direction == "<": cart.direction = "^"
                    elif direction == ">": cart.direction = "v"
                    elif direction == "v": cart.direction = "<"
                    else: cart.direction = ">"
            elif nextCh == "/":
                if direction == "<": cart.direction = "v"
                elif direction == ">": cart.direction = "^"
                elif direction == "v": cart.direction = "<"
                else: cart.direction = ">"
            elif nextCh == "\\":
                if direction == "<": cart.direction = "^"
                elif direction == ">": cart.direction = "v"
                elif direction == "v": cart.direction = ">"
                else: cart.direction = "<"
        for cart in deadCarts: self.carts.pop(cart)

    def HandleInput(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT]: 
            cartList = list(self.carts.keys())
            if self.target not in cartList: self.target = cartList[0]
            else:
                self.carts[self.target].manual = False
                index = cartList.index(self.target)
                if index > 0: self.target = cartList[index - 1]
                else: self.target = cartList[len(cartList) - 1]
            self.carts[self.target].manual = True
            self.carts[self.target].nextTurn = "u"
        if keys[pygame.K_RIGHT]: 
            cartList = list(self.carts.keys())
            if self.target not in cartList: self.target = cartList[0]
            else:
                self.carts[self.target].manual = False
                index = cartList.index(self.target)
                if index < len(cartList) - 1: self.target = cartList[index + 1]
                else: self.target = cartList[0]
            self.carts[self.target].manual = True
            self.carts[self.target].nextTurn = "u"
        if keys[pygame.K_UP]:
            self.tickRate -= 25
        if keys[pygame.K_DOWN]:
            self.tickRate += 25
        if keys[pygame.K_KP8]: camera.y -= 1; self.target = '.'
        if keys[pygame.K_KP5]: camera.y += 1; self.target = '.'
        if keys[pygame.K_KP4]: camera.x -= 1; self.target = '.'
        if keys[pygame.K_KP6]: camera.x += 1; self.target = '.'
        if keys[pygame.K_a]: self.carts[self.target].nextTurn = "l"
        if keys[pygame.K_d]: self.carts[self.target].nextTurn = "r"

    def Run(self):
        pygame.init()
        exitGame = False
        while len(self.carts) > 1 and not exitGame:
            startTime = pygame.time.get_ticks()
            for event in pygame.event.get():
                if event.type == pygame.QUIT: exitGame = True
            self.HandleInput()
            self.GameLoop()
            self.Draw()
            endTime = pygame.time.get_ticks()
            while endTime - startTime < self.tickRate:
                endTime = pygame.time.get_ticks()
        pygame.quit()


if __name__ == "__main__":
    game = Game()
    game.Run()